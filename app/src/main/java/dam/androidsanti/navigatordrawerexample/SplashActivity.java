package dam.androidsanti.navigatordrawerexample;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (Build.VERSION.SDK_INT > 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        Thread timer = new Thread() {

            @Override
            public void run() {

                super.run();
                try {
                    sleep(2000);
                } catch (InterruptedException e) {


                } finally {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                }
            }
        };
        timer.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        finish();
    }
}