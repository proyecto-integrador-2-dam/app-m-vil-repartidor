package dam.androidsanti.navigatordrawerexample;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import dam.androidsanti.navigatordrawerexample.dao.DeliveryManAPI;
import dam.androidsanti.navigatordrawerexample.data.OrdersListDBManager;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.DeliveryMan;

public class LoginActivity extends AppCompatActivity {

    private EditText user;
    private EditText passw;
    private Button btEnter;
    private static DeliveryMan repartidor;
    private DeliveryManAPI deliveryDao;
    private OrdersListDBManager ordersListDBManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        user = findViewById(R.id.etUsuario);
        passw = findViewById(R.id.etContrasena);
        btEnter = findViewById(R.id.btEnter);


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        deliveryDao = new DeliveryManAPI();
        repartidor = new DeliveryMan();

        ordersListDBManager = new OrdersListDBManager(getApplicationContext());
        ordersListDBManager.deleteAll();

        btEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!user.getText().toString().isEmpty() && !passw.getText().toString().isEmpty()) {

                    try {

                        if ((repartidor = deliveryDao.findByIdandPassw(Integer.valueOf(user.getText().toString()), passw.getText().toString())) != null){

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.putExtra("deliveryman", repartidor);
                            startActivity(intent);

                        }else{
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.incorrectPassw), Toast.LENGTH_SHORT).show();
                        }


                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.incorrectPassw), Toast.LENGTH_SHORT).show();
                    }
                }else {

                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.emptyPaswwd), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
