package dam.androidsanti.navigatordrawerexample;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;

import com.google.android.gms.maps.GoogleMap;

import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import dam.androidsanti.navigatordrawerexample.Ubication.GetUbicacion;
import dam.androidsanti.navigatordrawerexample.dao.AddressAPI;
import dam.androidsanti.navigatordrawerexample.dao.AlbaranAPI;
import dam.androidsanti.navigatordrawerexample.dao.CityAPI;
import dam.androidsanti.navigatordrawerexample.dao.ClientAPI;
import dam.androidsanti.navigatordrawerexample.dao.OrderAPI;
import dam.androidsanti.navigatordrawerexample.dao.PcAPI;
import dam.androidsanti.navigatordrawerexample.dao.RuteAPI;
import dam.androidsanti.navigatordrawerexample.data.OrderDataAsynchTask;
import dam.androidsanti.navigatordrawerexample.data.OrdersListDBManager;
import dam.androidsanti.navigatordrawerexample.ui.map.MapFragment;
import dam.androidsanti.navigatordrawerexample.ui.orders.MyAdapter;
import dam.androidsanti.navigatordrawerexample.ui.orders.OrderDetailActivity;
import dam.androidsanti.navigatordrawerexample.ui.orders.OrdersFragment;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Address;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Albaran;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.City;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Client;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.DeliveryMan;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Order;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Pc;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Rute;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener {

    private AppBarConfiguration mAppBarConfiguration;
    private OrderDataAsynchTask orderData;
    private ListView orderList;
    public static DeliveryMan deliveryMan;
    public ProgressBar progressBar;
    NavigationView navigationView;
    NavController navController;

    private static String[] PERMISSIONS_LOCATION = {Manifest.permission.ACCESS_FINE_LOCATION};
    private static final int REQUEST_LOCATION= 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        checkPermissions();

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            startService();
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_orders, R.id.nav_map, R.id.nav_logout)
                .setDrawerLayout(drawer)
                .build();

        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        deliveryMan = (DeliveryMan) getIntent().getSerializableExtra("deliveryman");

        View v = navigationView.getHeaderView(0);
        TextView username = v.findViewById(R.id.textViewDeliveryMan);
        username.setText(deliveryMan.getNombre()+" "+deliveryMan.getApellidos());

        navigationView.getMenu().findItem(R.id.nav_logout).setOnMenuItemClickListener(MenuItem -> {

            new AlertDialog.Builder(this)
                    .setMessage(R.string.exit)
                    .setPositiveButton(R.string.yes, (dialog, which) -> System.exit(0))

                    .setNegativeButton(R.string.cancel, null)
                    .show();

        return true;
        });

    }

    private void startService() {
        JobScheduler jobScheduler=(JobScheduler)getSystemService(JOB_SCHEDULER_SERVICE);
        JobInfo.Builder builder=new JobInfo.Builder(1,new ComponentName(this, GetUbicacion.class));

        builder.setMinimumLatency(30000);
        builder.setOverrideDeadline(TimeUnit.MILLISECONDS.toMillis(15));
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_NOT_ROAMING);
        builder.setBackoffCriteria(TimeUnit.MILLISECONDS.toMillis(10),JobInfo.BACKOFF_POLICY_LINEAR);
        builder.setRequiresCharging(false);

        int result = jobScheduler.schedule(builder.build());

        if(result==JobScheduler.RESULT_SUCCESS){
            Log.e("SERVICIO","JOB SERVICE SUCCES");
        }else{
            Log.e("SERVICIO","JOB SERVICE SUCCES");
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.fab:

                MapFragment fr = new MapFragment();

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.nav_host_fragment, fr)
                        .addToBackStack(null)
                        .commit();
                break;

            default:

                OrdersFragment fr2 = new OrdersFragment();

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.nav_host_fragment, fr2)
                        .addToBackStack(null)
                        .commit();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            MapFragment test = new MapFragment();

            try {
                test = (MapFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);

            } catch (Exception e) {

            } finally {


                if (test != null && test.isVisible()) {
                    OrdersFragment fr2 = new OrdersFragment();

                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.nav_host_fragment, fr2)
                            .addToBackStack(null)
                            .commit();
                } else {
                    new AlertDialog.Builder(this)
                            .setMessage(R.string.exit)
                            .setPositiveButton(R.string.yes, (dialog, which) -> System.exit(0))

                            .setNegativeButton(R.string.cancel, null)
                            .show();
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public void onItemClick(Order order) {
        Intent intent = new Intent(getApplicationContext(), OrderDetailActivity.class);

        intent.putExtra("order", order);
        startActivity(intent);
    }

    //check if the network is available
    private boolean networkAvailable() {
        Boolean available = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                available = true;
            }
        }

        return available;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.mapNormal:
                MapFragment.changeMap(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.mapHybrid:
                MapFragment.changeMap(GoogleMap.MAP_TYPE_HYBRID);
                break;
            case R.id.mapSat:
                MapFragment.changeMap(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.mapTerrain:
                MapFragment.changeMap(GoogleMap.MAP_TYPE_TERRAIN);
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, PERMISSIONS_LOCATION, REQUEST_LOCATION);
            return false;
        } else {
            MapFragment.checkPermissions=true;

            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                MapFragment.checkPermissions=true;
            }
            else {
                MapFragment.checkPermissions=false;

                Toast.makeText(this, getString(R.string.permissionMessage), Toast.LENGTH_LONG).show();
            }
        } else {

            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}