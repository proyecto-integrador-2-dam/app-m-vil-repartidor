package dam.androidsanti.navigatordrawerexample.conexion;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import java.net.MalformedURLException;
import java.net.URL;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;

public class ConexionOdoo {

    private static XmlRpcClient client;
    public static final String DB = "batoilogic";
    public static final String USER = "grup3@batoilogic.es";
    public static final String PWD = "grup3-3333";
    public static int uid;

    public static XmlRpcClient getInstance() {
        if (client == null) {

            client = new XmlRpcClient();

            XmlRpcClientConfigImpl configBD = new XmlRpcClientConfigImpl();
            configBD.setEnabledForExtensions(true);
            try {
                configBD.setServerURL(new URL("http", "grup3.cipfpbatoi.es", 80, "/xmlrpc/2/object"));


                client.setConfig(configBD);

                XmlRpcClientConfigImpl auth = new XmlRpcClientConfigImpl();
                auth.setEnabledForExtensions(true);
                auth.setServerURL(new URL("http", "grup3.cipfpbatoi.es", 80, "/xmlrpc/2/common"));

                uid = (int) client.execute(auth, "authenticate", asList(DB, USER, PWD, emptyMap()));

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (XmlRpcException e) {
                e.printStackTrace();
            }
        }

        return client;
    }
}
