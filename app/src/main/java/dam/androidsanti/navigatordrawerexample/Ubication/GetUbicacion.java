package dam.androidsanti.navigatordrawerexample.Ubication;

import android.Manifest;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import dam.androidsanti.navigatordrawerexample.MainActivity;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class GetUbicacion extends JobService
{
    private static final int BUFFER_SIZE = 1024;
    private static final int SO_TIMEOUT = 9000;
    private static final String SERVER= "grup3.cipfpbatoi.es";
    private static final int PORT = 9000;
    private LocationManager locationManager;

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d("UBICACION","JobStarted");
        doService();
        doJob(params);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d("UBICACION","Job cancelled before completion");
        return true;
    }

    private void doJob(final JobParameters parameters) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                int toport=PORT;

                try(DatagramSocket datagramSocket = new DatagramSocket()) {


                    datagramSocket.setSoTimeout(SO_TIMEOUT);
                    send(datagramSocket,InetAddress.getByName(SERVER),toport);


                } catch (SocketException e) {
                    e.printStackTrace();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.d("UBICACION","Job Finished");
                jobFinished(parameters,false);
            }
        }).start();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void doService() {
        JobScheduler jobScheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        JobInfo.Builder builder = new JobInfo.Builder(777, new ComponentName(this, GetUbicacion.class));  //Specify which JobService performs the operation
        builder.setMinimumLatency(60000); //Minimum latency of execution
        builder.setOverrideDeadline(60000);  //Maximum latency of execution
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_NOT_ROAMING);  //Non-roaming network state
        builder.setBackoffCriteria(60000, JobInfo.BACKOFF_POLICY_LINEAR);  //Linear Retry Scheme
        builder.setRequiresCharging(false); // Uncharged state
        int resultCode = jobScheduler.schedule(builder.build());

        if(resultCode == JobScheduler.RESULT_SUCCESS)
            Log.d("UBICACION","JOB SCHEDULED");
        else
            Log.d("UBICACION","JOB SCHEDULED FAILED");
    }

    private void send(DatagramSocket datagramSocket, InetAddress toServer, int toPort) throws IOException,Exception {

        Location location = null;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        //Info id latitud longitud
        Log.d("UBICACION",location.getLongitude()+" "+location.getLatitude());

        int repartidorId = MainActivity.deliveryMan.getId();

        String info= repartidorId+":"+location.getLatitude()+":"+location.getLongitude();

        DatagramPacket datagramPacket = new DatagramPacket(info.getBytes(),info.getBytes().length,toServer,toPort);
        datagramSocket.send(datagramPacket);

        byte[] response = new byte[100];
        DatagramPacket datagramPacketResponse = new DatagramPacket(response,response.length);
        datagramSocket.receive(datagramPacketResponse);
        String aux = new String(datagramPacketResponse.getData()).trim();

        if(aux.equals("received"))
            System.out.println("The client received the message");
        else
            throw new Exception("PROTOCOL ERROR: Server sent not valid token: "+aux);
    }
}