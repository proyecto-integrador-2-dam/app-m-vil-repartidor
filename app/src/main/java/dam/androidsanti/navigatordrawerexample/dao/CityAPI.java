package dam.androidsanti.navigatordrawerexample.dao;

import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dam.androidsanti.navigatordrawerexample.conexion.ConexionOdoo;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.City;

import static java.util.Arrays.asList;

public class CityAPI implements  GenericAPI<City> {

    private XmlRpcClient conexion;
    private City city;

    public CityAPI() {
        conexion = ConexionOdoo.getInstance();
        city = new City();
    }

    @Override
    public City findbyId(int id) throws Exception {

        List<Object> list;
        City c = new City();

        list = asList((Object[]) conexion.execute("execute_kw", asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PWD,
                "batoi_logic.ciudad", "search_read",
                asList(asList(
                        asList("id", "=", id))),
                new HashMap() {
                    {
                        put("fields", asList("nombre", "provincia"));
                    }
                }
        )));

        for (int i = 0; i < list.size(); i++) {
            HashMap offset = (HashMap) list.get(i);

            int idd = Integer.parseInt(offset.get("id") + "");
            String nombre = offset.get("nombre")+"";
            String province = offset.get("provincia")+"";


            c = new City(idd, nombre, province);
        }

        if(c.getId() == 0){

            return null;
        }else return c;
    }

    @Override
    public List<City> findAll() throws Exception {
        return null;
    }
}
