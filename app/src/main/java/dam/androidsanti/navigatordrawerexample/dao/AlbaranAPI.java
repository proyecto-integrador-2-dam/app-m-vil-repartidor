package dam.androidsanti.navigatordrawerexample.dao;

import android.os.StrictMode;
import android.util.Log;

import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dam.androidsanti.navigatordrawerexample.conexion.ConexionOdoo;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Albaran;

import static java.util.Arrays.asList;

public class AlbaranAPI implements GenericAPI<Albaran>{

    private XmlRpcClient conexion;
    private Albaran albaran;

    public AlbaranAPI() {
        conexion = ConexionOdoo.getInstance();
        albaran = new Albaran();
    }

    public ArrayList<Albaran> findAll(int idRute) throws Exception {
        List<Object> list;
        ArrayList<Albaran> fullList = new ArrayList<>();

        list = asList((Object[]) conexion.execute("execute_kw", asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PWD,
                "batoi_logic.albaran", "search_read",
                asList(asList(
                        asList("ruta", "=", idRute))),
                new HashMap() {
                    {
                        put("fields", asList("pedido", "ruta"));
                    }
                }
        )));

        for (int i = 0; i < list.size(); i++) {
            HashMap offset = (HashMap) list.get(i);

            int id = Integer.parseInt(offset.get("id") + "");
            M2OField auxPedido = getOne2Many(offset, "pedido");

            M2OField auxRuta = getOne2Many(offset, "ruta");

            Log.i("qwqw", id +" "+ auxPedido.id +" "+ auxRuta.id);
            fullList.add(new Albaran(id, auxPedido.id, auxRuta.id));
        }

        return fullList;
    }

    public static M2OField getOne2Many(Map<String, Object> classObj, String fieldname){

        Integer fieldId = 0;
        String fieldValue = "";
        M2OField res = new M2OField();

        if (classObj.get(fieldname) instanceof  Object[]){

            Object[] field = (Object[]) classObj.get(fieldname);

            if (field.length > 0){

                fieldId = (Integer) field[0];
                fieldValue= (String) field[1];
            }
        }

        res.id = fieldId;
        res.value = fieldValue;

        return res;
    }

    @Override
    public Albaran findbyId(int id) throws Exception {
        return null;
    }

    @Override
    public List<Albaran> findAll() throws Exception {
        return null;
    }

    public static class M2OField{

        public Integer id;
        public String value;

    }
}
