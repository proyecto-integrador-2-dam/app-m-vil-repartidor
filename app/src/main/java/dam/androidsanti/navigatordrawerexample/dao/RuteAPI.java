package dam.androidsanti.navigatordrawerexample.dao;

import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.HashMap;
import java.util.List;

import dam.androidsanti.navigatordrawerexample.conexion.ConexionOdoo;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Rute;

import static java.util.Arrays.asList;

public class RuteAPI implements GenericAPI<Rute>{

    private XmlRpcClient conexion;
    private Rute rute;

    public RuteAPI() {
        conexion = ConexionOdoo.getInstance();
        rute = new Rute();
    }

    @Override
    public Rute findbyId(int id) throws Exception {
        List<Object> list;
        Rute r = new Rute();

        list = asList((Object[]) conexion.execute("execute_kw", asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PWD,
                "batoi_logic.ruta", "search_read",
                asList(asList(
                        asList("repartidor", "=", id))),
                new HashMap() {
                    {
                        put("fields", asList("repartidor"));
                    }
                }
        )));

        for (int i = 0; i < list.size(); i++) {
            HashMap offset = (HashMap) list.get(i);

            int idd = Integer.parseInt(offset.get("id") + "");

            r = new Rute(idd, id);
        }

        if(r.getId() == 0){

            return null;
        }else return r;
    }

    @Override
    public List<Rute> findAll() throws Exception {
        return null;
    }
}
