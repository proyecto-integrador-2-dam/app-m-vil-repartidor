package dam.androidsanti.navigatordrawerexample.dao;

import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dam.androidsanti.navigatordrawerexample.conexion.ConexionOdoo;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Pc;

import static java.util.Arrays.asList;

public class PcAPI implements  GenericAPI<Pc>{

    private XmlRpcClient conexion;
    private Pc postalCode;

    public PcAPI() {
        conexion = ConexionOdoo.getInstance();
        postalCode = new Pc();
    }
    @Override
    public Pc findbyId(int id) throws Exception {
        List<Object> list;
        Pc pc = new Pc();

        list = asList((Object[]) conexion.execute("execute_kw", asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PWD,
                "batoi_logic.cp", "search_read",
                asList(asList(
                        asList("id", "=", id))),
                new HashMap() {
                    {
                        put("fields", asList("cp", "ciudad"));
                    }
                }
        )));

        for (int i = 0; i < list.size(); i++) {
            HashMap offset = (HashMap) list.get(i);

            int idd = Integer.parseInt(offset.get("id") + "");
            int cp = Integer.parseInt(offset.get("cp")+"");
            M2OField auxCity = getOne2Many(offset, "ciudad");

            pc = new Pc(idd, cp, auxCity.id);
        }

        if(pc.getId() == 0){

            return null;
        }else return pc;

    }

    @Override
    public List<Pc> findAll() throws Exception {
        return null;
    }

    public static M2OField getOne2Many(Map<String, Object> classObj, String fieldname){

        Integer fieldId = 0;
        String fieldValue = "";
        M2OField res = new M2OField();

        if (classObj.get(fieldname) instanceof  Object[]){

            Object[] field = (Object[]) classObj.get(fieldname);

            if (field.length > 0){

                fieldId = (Integer) field[0];
                fieldValue= (String) field[1];
            }
        }

        res.id = fieldId;
        res.value = fieldValue;

        return res;
    }

    public static class M2OField{

        public Integer id;
        public String value;

    }
}
