package dam.androidsanti.navigatordrawerexample.dao;

import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dam.androidsanti.navigatordrawerexample.conexion.ConexionOdoo;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.DeliveryMan;

import static java.util.Arrays.asList;

public class DeliveryManAPI implements GenericAPI<DeliveryMan>{
    private XmlRpcClient conexion;
    private DeliveryMan deliveryMan;

    public DeliveryManAPI() {
        conexion = ConexionOdoo.getInstance();
        deliveryMan = new DeliveryMan();
    }

    @Override
    public DeliveryMan findbyId(int id) throws Exception {
        return null;
    }

    public ArrayList<DeliveryMan> findAll() throws Exception {
        List<Object> list;
        ArrayList<DeliveryMan> fullList = new ArrayList<>();

        list = asList((Object[]) conexion.execute("execute_kw", asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PWD,
                "batoi_logic.repartidor", "search_read",
                asList(),
                new HashMap() {
                    {
                        put("fields", asList("nombre", "apellidos", "passw"));
                    }
                }
        )));

        for (int i = 0; i < list.size(); i++) {
            HashMap offset = (HashMap) list.get(i);

            int id = Integer.parseInt(offset.get("id") + "");
            String nombre = offset.get("nombre") + "";
            String apellidos = offset.get("apellidos") + "";
            String passw = offset.get("passw") + "";

            fullList.add(new DeliveryMan(id, nombre, apellidos, passw));
        }

        return fullList;
    }

    public DeliveryMan findByIdandPassw(Integer idDM, String passwd) throws Exception {
        List<Object> list;
        DeliveryMan dm = new DeliveryMan();

        list = asList((Object[]) conexion.execute("execute_kw", asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PWD,
                "batoi_logic.repartidor", "search_read",
                asList(asList(
                        asList("id", "=", idDM),
                        asList("passw", "=", passwd))),
                new HashMap() {
                    {
                        put("fields", asList("nombre", "apellidos", "passw"));
                    }
                }
        )));

        for (int i = 0; i < list.size(); i++) {
            HashMap offset = (HashMap) list.get(i);

            int id = Integer.parseInt(offset.get("id") + "");
            String nombre = offset.get("nombre") + "";
            String apellidos = offset.get("apellidos") + "";
            String passw = offset.get("passw") + "";

            dm = new DeliveryMan(id, nombre, apellidos, passw);
        }

        if(dm.getId() == 0){

            return null;
        }else return dm;

    }
}
