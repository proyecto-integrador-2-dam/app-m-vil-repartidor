package dam.androidsanti.navigatordrawerexample.dao;

import android.util.Log;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dam.androidsanti.navigatordrawerexample.conexion.ConexionOdoo;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Order;

import static java.util.Arrays.asList;

public class OrderAPI implements GenericAPI<Order>{

    private XmlRpcClient conexion;
    private Order order;

    public OrderAPI() {
        conexion = ConexionOdoo.getInstance();
        order = new Order();
    }

    @Override
    public Order findbyId(int id) throws Exception {
        List<Object> list;
        Order r = new Order();

        list = asList((Object[]) conexion.execute("execute_kw", asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PWD,
                "batoi_logic.pedido", "search_read",
                asList(asList(
                        asList("id", "=", id))),
                new HashMap() {
                    {
                        put("fields", asList("estado", "observaciones", "cliente", "direccion"));
                    }
                }
        )));

        for (int i = 0; i < list.size(); i++) {
            HashMap offset = (HashMap) list.get(i);

            int idd = Integer.parseInt(offset.get("id") + "");
            String estado = offset.get("estado")+"";
            String observaciones = offset.get("observaciones")+"";
            M2OField auxCliente = getOne2Many(offset, "cliente");
            M2OField auxDireccion = getOne2Many(offset, "direccion");

            Log.i("qwqw", idd +" "+estado +" "+observaciones+" "+auxCliente.id+" "+auxDireccion.id);
            r = new Order(idd, estado, observaciones, String.valueOf(auxCliente.id), String.valueOf(auxDireccion.id));
        }

        if(r.getIdPedido() == 0){

            return null;
        }else return r;

    }

    public Order insert(int id, String state, String reason) throws Exception {

        conexion.execute("execute_kw", asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PWD,
                "batoi_logic.pedido", "write",
                asList(
                        asList(id),
                        new HashMap() {
                            {
                                put("estado", state);
                                put("observaciones", reason);
                            }
                        }
                )
        ));
        return null;
    }

    @Override
    public List<Order> findAll() throws Exception {
        return null;
    }

    public static M2OField getOne2Many(Map<String, Object> classObj, String fieldname){

        Integer fieldId = 0;
        String fieldValue = "";
        M2OField res = new M2OField();

        if (classObj.get(fieldname) instanceof  Object[]){

            Object[] field = (Object[]) classObj.get(fieldname);

            if (field.length > 0){

                fieldId = (Integer) field[0];
                fieldValue= (String) field[1];
            }
        }

        res.id = fieldId;
        res.value = fieldValue;

        return res;
    }

    public static class M2OField{

        public Integer id;
        public String value;

    }
}
