package dam.androidsanti.navigatordrawerexample.dao;

import java.util.List;

public interface GenericAPI<Obj> {

    Obj findbyId(int id) throws Exception;

    List<Obj> findAll() throws Exception;

}
