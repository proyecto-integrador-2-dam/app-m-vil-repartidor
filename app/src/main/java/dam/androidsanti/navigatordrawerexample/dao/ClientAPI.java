package dam.androidsanti.navigatordrawerexample.dao;

import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.HashMap;
import java.util.List;

import dam.androidsanti.navigatordrawerexample.conexion.ConexionOdoo;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Client;

import static java.util.Arrays.asList;

public class ClientAPI implements GenericAPI<Client>{

    private XmlRpcClient conexion;
    private Client client;

    public ClientAPI() {

        conexion = ConexionOdoo.getInstance();
        client = new Client();
    }

    @Override
    public Client findbyId(int id) throws Exception {

        List<Object> list;
        Client c = new Client();

        list = asList((Object[]) conexion.execute("execute_kw", asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PWD,
                "batoi_logic.cliente", "search_read",
                asList(asList(
                        asList("id", "=", id))),
                new HashMap() {
                    {
                        put("fields", asList("nombre", "apellidos"));
                    }
                }
        )));

        for (int i = 0; i < list.size(); i++) {
            HashMap offset = (HashMap) list.get(i);

            int idd = Integer.parseInt(offset.get("id") + "");
            String name = offset.get("nombre")+"";
            String surnames = offset.get("apellidos")+"";

            c = new Client(idd, name, surnames);
        }

        if(c.getId() == 0){

            return null;
        }else return c;
    }

    @Override
    public List<Client> findAll() throws Exception {
        return null;
    }
}
