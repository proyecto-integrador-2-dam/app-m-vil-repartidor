package dam.androidsanti.navigatordrawerexample.dao;

import android.util.Log;

import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dam.androidsanti.navigatordrawerexample.conexion.ConexionOdoo;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Address;

import static java.util.Arrays.asList;

public class AddressAPI implements  GenericAPI<Address> {

    private XmlRpcClient conexion;
    private Address address;

    public AddressAPI() {
        conexion = ConexionOdoo.getInstance();
        address = new Address();
    }
    @Override
    public Address findbyId(int id) throws Exception {
        List<Object> list;
        Address a = new Address();

        list = asList((Object[]) conexion.execute("execute_kw", asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PWD,
                "batoi_logic.direccion", "search_read",
                asList(asList(
                        asList("id", "=", id))),
                new HashMap() {
                    {
                        put("fields", asList("calle", "cp"));
                    }
                }
        )));

        for (int i = 0; i < list.size(); i++) {
            HashMap offset = (HashMap) list.get(i);

            int idd = Integer.parseInt(offset.get("id") + "");
            String street = offset.get("calle")+"";
            M2OField auxPC = getOne2Many(offset, "cp");

            a = new Address(idd, street, auxPC.id);
        }

        if(a.getId() == 0){

            return null;
        }else return a;

    }

    @Override
    public List<Address> findAll() throws Exception {
        return null;
    }

    public static M2OField getOne2Many(Map<String, Object> classObj, String fieldname){

        Integer fieldId = 0;
        String fieldValue = "";
        M2OField res = new M2OField();

        if (classObj.get(fieldname) instanceof  Object[]){

            Object[] field = (Object[]) classObj.get(fieldname);

            if (field.length > 0){

                fieldId = (Integer) field[0];
                fieldValue= (String) field[1];
            }
        }

        res.id = fieldId;
        res.value = fieldValue;

        return res;
    }

    public static class M2OField{

        public Integer id;
        public String value;

    }
}
