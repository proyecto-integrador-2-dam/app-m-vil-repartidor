package dam.androidsanti.navigatordrawerexample.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import dam.androidsanti.navigatordrawerexample.ui.orders.models.Order;

public class OrdersListDBManager {
    private OrdersListDBHelper ordersListDBHelper;

    public OrdersListDBManager(Context context) {
        ordersListDBHelper = OrdersListDBHelper.getInstance(context);
    }

    //Operations

    public void insert(Integer idPedido, String status, String obser , String client, String address) {

        SQLiteDatabase sqLiteDatabase = ordersListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null) {

            ContentValues contentValues = new ContentValues();

            contentValues.put(OrdersListDBContract.Pedidos.IDPEDIDO, idPedido);
            contentValues.put(OrdersListDBContract.Pedidos.ESTADO, status);
            contentValues.put(OrdersListDBContract.Pedidos.OBSERVACIONES, obser);
            contentValues.put(OrdersListDBContract.Pedidos.CLIENTE, client);
            contentValues.put(OrdersListDBContract.Pedidos.DIRECCION, address);

            sqLiteDatabase.insert(OrdersListDBContract.Pedidos.TABLE_NAME, null, contentValues);

        }
    }

    public void updateOrder(String state, String reason, String id) {

        SQLiteDatabase sqLiteDatabase = ordersListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null) {
            sqLiteDatabase.execSQL("UPDATE PEDIDOS SET estado='"+state+"', observaciones='"+reason+"' WHERE idpedido='"+id+"'");
        }
    }


    public void deleteAll() {
        SQLiteDatabase sqLiteDatabase = ordersListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null) {
            sqLiteDatabase.execSQL("delete from PEDIDOS");
        }
    }

    public ArrayList<Order> getOrders() {
        ArrayList<Order> taskList = new ArrayList<>();

        SQLiteDatabase sqLiteDatabase = ordersListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {

            String[] projection = new String[]{OrdersListDBContract.Pedidos._ID, OrdersListDBContract.Pedidos.IDPEDIDO,
                    OrdersListDBContract.Pedidos.ESTADO, OrdersListDBContract.Pedidos.OBSERVACIONES,
                    OrdersListDBContract.Pedidos.CLIENTE, OrdersListDBContract.Pedidos.DIRECCION};

            Cursor cursorTodoList = sqLiteDatabase.query(OrdersListDBContract.Pedidos.TABLE_NAME, projection,
                    null, null, null, null, null);

            if (cursorTodoList != null) {

                int _idIndex = cursorTodoList.getColumnIndexOrThrow(OrdersListDBContract.Pedidos._ID);
                int idPedido = cursorTodoList.getColumnIndexOrThrow(OrdersListDBContract.Pedidos.IDPEDIDO);
                int todoIndex = cursorTodoList.getColumnIndexOrThrow(OrdersListDBContract.Pedidos.ESTADO);
                int to_AccomplishIndex = cursorTodoList.getColumnIndexOrThrow(OrdersListDBContract.Pedidos.OBSERVACIONES);
                int priority = cursorTodoList.getColumnIndexOrThrow(OrdersListDBContract.Pedidos.CLIENTE);
                int status = cursorTodoList.getColumnIndexOrThrow(OrdersListDBContract.Pedidos.DIRECCION);

                while (cursorTodoList.moveToNext()) {

                    Order task = new Order(
                            /*Integer.parseInt(cursorTodoList.getString(_idIndex)),*/Integer.parseInt(cursorTodoList.getString(idPedido)), cursorTodoList.getString(todoIndex),
                            cursorTodoList.getString(to_AccomplishIndex), cursorTodoList.getString(priority),
                            cursorTodoList.getString(status)
                    );

                    taskList.add(task);
                }

                cursorTodoList.close();
            }
        }

        return taskList;
    }


    public void close() {
        ordersListDBHelper.close();   // closes any opened database object
    }
}