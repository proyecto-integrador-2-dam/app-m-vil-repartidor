package dam.androidsanti.navigatordrawerexample.data;


/*
Class that defines TodoListDB schema
 */
public final class OrdersListDBContract {
    //Common fields to all DB

    //Database Name
    public static final String DB_NAME = "REPARTIDOR.DB";
    //Database Version
    public static final int DB_VERSION = 1;

    /*
    To prevent someone from accidentally instantiating the contract class
    make the constructor private
     */
    private OrdersListDBContract() {
    }

    //schema

    //TABLE TASK: Inner class that defines the table Task contents
    public static class Pedidos {
        //Table name
        public static final String TABLE_NAME = "PEDIDOS";

        //Columns names
        public static final String _ID = "_id";
        public static final String IDPEDIDO = "idpedido";
        public static final String ESTADO = "estado";
        public static final String OBSERVACIONES = "observaciones";
        public static final String CLIENTE = "cliente";
        public static final String DIRECCION = "direccion";

        //CREATE_TABLE SQL String
        public static final String CREATE_TABLE = "CREATE TABLE " + Pedidos.TABLE_NAME
                + " ("
                + Pedidos._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Pedidos.IDPEDIDO + " TEXT NOT NULL, "
                + Pedidos.ESTADO + " TEXT NOT NULL, "
                + Pedidos.OBSERVACIONES + " TEXT, "
                + Pedidos.CLIENTE + " TEXT NOT NULL, "
                + Pedidos.DIRECCION + " TEXT"
                /*+ Pedidos.LAT + " FLOAT, "
                + Pedidos.LAT + " FLOAT, "*/
                + ");";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + Pedidos.TABLE_NAME;

        // WHERE CONDITIONS to filter the tasks


        //DELETE sentences

        //other table definition would come here

    }

}
















