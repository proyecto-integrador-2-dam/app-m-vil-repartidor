package dam.androidsanti.navigatordrawerexample.data;

import android.content.Context;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import dam.androidsanti.navigatordrawerexample.MainActivity;
import dam.androidsanti.navigatordrawerexample.R;
import dam.androidsanti.navigatordrawerexample.dao.AddressAPI;
import dam.androidsanti.navigatordrawerexample.dao.AlbaranAPI;
import dam.androidsanti.navigatordrawerexample.dao.CityAPI;
import dam.androidsanti.navigatordrawerexample.dao.ClientAPI;
import dam.androidsanti.navigatordrawerexample.dao.OrderAPI;
import dam.androidsanti.navigatordrawerexample.dao.PcAPI;
import dam.androidsanti.navigatordrawerexample.dao.RuteAPI;
import dam.androidsanti.navigatordrawerexample.ui.orders.MyAdapter;
import dam.androidsanti.navigatordrawerexample.ui.orders.OrdersFragment;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Address;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Albaran;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.City;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Client;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Order;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Pc;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Rute;

public class OrderDataAsynchTask extends AsyncTask<URL, Void, ArrayList<Order>> {

    private final int CONNECTION_TIME_OUT = 15000;
    private final int READ_TIME_OUT = 10000;

    private RecyclerView list;
    private Rute rute;
    private RuteAPI rutedao;
    private Albaran albaran;
    private AlbaranAPI albarandao;
    private ArrayList<Albaran> listAlbaranes;
    private Order order;
    private OrderAPI orderdao;
    public static ArrayList<Order> listaOrders;
    private OrdersListDBManager sqlite;
    private Client client;
    private ClientAPI clientdao;
    private ArrayList<Client> listaClients;
    private Address address;
    private AddressAPI addressdao;
    private ArrayList<Address> addressList;
    private Pc pc;
    private PcAPI pcdao;
    private ArrayList<Pc> pcList;
    private City city;
    private CityAPI citydao;
    private ArrayList<City> citiesList;
    private Context context;
    private ProgressBar progressBar;

    public OrderDataAsynchTask(RecyclerView list, Context context, ProgressBar progressBar) {
        this.list = list;
        this.context = context;
        this.progressBar = progressBar;
    }

    @Override
    protected ArrayList<Order> doInBackground(URL... urls) {
        //obtener los datos desde postgre / odoo api
        ArrayList<Order> lista = insertOnSQLite();
        return lista;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPostExecute(ArrayList<Order> orders) {
        super.onPostExecute(orders);

        progressBar.setVisibility(View.INVISIBLE);
        OrdersFragment.datosCargados();
    }

    public ArrayList<Order> insertOnSQLite() {

        try {

            rute = new Rute();
            rutedao = new RuteAPI();
            rute = rutedao.findbyId(MainActivity.deliveryMan.getId());


            albaran = new Albaran();
            albarandao = new AlbaranAPI();

            listAlbaranes = albarandao.findAll(rute.getId());


            order = new Order();
            orderdao = new OrderAPI();
            listaOrders = new ArrayList<>();

            for (int i = 0; i < listAlbaranes.size(); i++) {
                listaOrders.add(orderdao.findbyId(listAlbaranes.get(i).pedido));
            }


            client = new Client();
            clientdao = new ClientAPI();
            listaClients = new ArrayList<>();
            address = new Address();
            addressdao = new AddressAPI();
            addressList = new ArrayList<>();
            pc = new Pc();
            pcdao = new PcAPI();
            pcList = new ArrayList<>();
            city = new City();
            citydao = new CityAPI();
            citiesList = new ArrayList<>();

            for (int i = 0; i < listaOrders.size(); i++) {

                listaClients.add(clientdao.findbyId(Integer.parseInt(listaOrders.get(i).getCliente())));
                addressList.add(addressdao.findbyId(Integer.parseInt(listaOrders.get(i).getDireccion())));
                pcList.add(pcdao.findbyId(addressList.get(i).getPc()));
                citiesList.add(citydao.findbyId(pcList.get(i).getCiudad()));
            }

            for (int i = 0; i<listaOrders.size(); i++){

                Geocoder geocoder = new Geocoder(context, Locale.getDefault());

                List<android.location.Address> addresses = geocoder.getFromLocationName(addressList.get(i).getStreet() + " " + citiesList.get(i).getNombre() + " " +
                        citiesList.get(i).getProvinvia(), 1);

                if(addresses.size() > 0){

                    listaOrders.get(i).setLatitud(addresses.get(0).getLatitude());
                    listaOrders.get(i).setLongitud(addresses.get(0).getLongitude());
                }

                listaOrders.get(i).setCliente(listaClients.get(i).getName() + " " + listaClients.get(i).getSurnames());
            }

            sqlite = new OrdersListDBManager(context);
            for (int i = 0; i < listaOrders.size(); i++) {

                sqlite.insert(listaOrders.get(i).getIdPedido(), listaOrders.get(i).getEstado(), listaOrders.get(i).getObservaciones(),
                        listaOrders.get(i).getCliente(), addressList.get(i).getStreet() + ",  " + citiesList.get(i).getNombre() + ",  " +
                                citiesList.get(i).getProvinvia());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listaOrders;

    }
}
