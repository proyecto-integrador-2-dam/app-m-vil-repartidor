package dam.androidsanti.navigatordrawerexample.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class OrdersListDBHelper extends SQLiteOpenHelper {
    //instance to SQLiteOpenHelper
    private static OrdersListDBHelper instanceDBHelper;

    /**
     * This method assures only one instance of OrdersListDBHelper for all the application.
     *
     * @param context use this context to not leak Activity context
     * @return only one instance
     */
    public static synchronized OrdersListDBHelper getInstance(Context context) {
        //instance must be unique
        if (instanceDBHelper == null) {
            instanceDBHelper = new OrdersListDBHelper(context.getApplicationContext());
        }

        return instanceDBHelper;
    }

    public OrdersListDBHelper(Context context) {
        super(context, OrdersListDBContract.DB_NAME, null, OrdersListDBContract.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(OrdersListDBContract.Pedidos.CREATE_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // the upgrade policy is simply discard the table and start over
        sqLiteDatabase.execSQL(OrdersListDBContract.Pedidos.DELETE_TABLE);
        //create again the DB
        onCreate(sqLiteDatabase);
    }
}