package dam.androidsanti.navigatordrawerexample.ui.orders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.androidsanti.navigatordrawerexample.R;
import dam.androidsanti.navigatordrawerexample.data.OrdersListDBManager;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Order;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private OrdersListDBManager ordersListDBManager;
    private ArrayList<Order> myDataSet;
    private OnItemClickListener listener;

    public interface OnItemClickListener {

        void onItemClick(Order order);
    }


    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView nombreDestinatario;
        TextView direccion;
        TextView estado;
        CardView cv;
        ConstraintLayout constraintCV;
        ImageView statusImg;

        public MyViewHolder(View view) {
            super(view);
            nombreDestinatario = view.findViewById(R.id.tvNombreDestinatario);
            direccion = view.findViewById(R.id.tvDireccion);
            estado = view.findViewById(R.id.tvIdPedido);
            cv = view.findViewById(R.id.cv);
            constraintCV = view.findViewById(R.id.contraintCV);
            statusImg = view.findViewById(R.id.stateImg);
        }

        public void bind(Order order, final OnItemClickListener listener) {
            this.nombreDestinatario.setText(order.getCliente());
            this.direccion.setText(order.getDireccion());
            this.estado.setText(order.getEstado());
            this.cv.setOnClickListener(v -> listener.onItemClick(order));

            if(estado.getText().equals(this.nombreDestinatario.getResources().getString(R.string.delivered))){

                statusImg.setVisibility(View.VISIBLE);
                statusImg.setImageResource(R.drawable.check_green);
            }
            if(estado.getText().equals(this.nombreDestinatario.getResources().getString(R.string.noDelivered))){

                statusImg.setVisibility(View.VISIBLE);
                statusImg.setImageResource(R.drawable.cancel_status);
            }
        }
    }

    public void getData() {
        this.myDataSet = ordersListDBManager.getOrders();
        notifyDataSetChanged();

    }

    public MyAdapter(OrdersListDBManager ordersListDBManager, OnItemClickListener listener) {
        this.ordersListDBManager = ordersListDBManager;
        this.listener = listener;
        //this.myDataSet = ordersListDBManager.getOrders();
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View tv = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);

        return new MyViewHolder(tv);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {

        viewHolder.bind(myDataSet.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return myDataSet.size();
    }


}
