package dam.androidsanti.navigatordrawerexample.ui.orders.models;

public class City {

    public int id;
    public String nombre;
    public String provinvia;

    public City() {
    }

    public City(int id, String nombre, String provinvia) {
        this.id = id;
        this.nombre = nombre;
        this.provinvia = provinvia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getProvinvia() {
        return provinvia;
    }

    public void setProvinvia(String provinvia) {
        this.provinvia = provinvia;
    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", provinvia='" + provinvia + '\'' +
                '}';
    }
}
