package dam.androidsanti.navigatordrawerexample.ui.orders;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import dam.androidsanti.navigatordrawerexample.MainActivity;
import dam.androidsanti.navigatordrawerexample.R;
import dam.androidsanti.navigatordrawerexample.data.OrderDataAsynchTask;
import dam.androidsanti.navigatordrawerexample.data.OrdersListDBManager;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.DeliveryMan;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Order;

public class OrdersFragment extends Fragment implements MyAdapter.OnItemClickListener {

    public static RecyclerView recyclerView;
    private OrderDataAsynchTask orderData;
    public static MyAdapter myAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private FloatingActionButton fab;
    private FloatingActionButton fab2;
    private OrdersListDBManager ordersListDBManager;
    public ProgressBar progressBar;
    private static boolean chargeOrders=true;
    private View root;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_orders, container, false);

        ((MainActivity) getActivity()).setActionBarTitle(getString(R.string.menu_orders));
        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);

        fab = getActivity().findViewById(R.id.fab);
        fab.show();
        fab2 = getActivity().findViewById(R.id.fab2);
        fab2.hide();


        progressBar = root.findViewById(R.id.progressBar);

        setUI();
        return root;
    }

    public static void datosCargados() {
        myAdapter.getData();
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onItemClick(Order order) {

        Intent intent = new Intent(getContext(), OrderDetailActivity.class);
        intent.putExtra("nombredestinatario", order.getCliente());
        intent.putExtra("direccion", order.getDireccion());
        intent.putExtra("estado", order.getEstado());
        intent.putExtra("observaciones", order.getObservaciones());
        intent.putExtra("idpedido", order.getIdPedido());

        startActivity(intent);
    }

    private void setUI() {


        layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        ordersListDBManager = new OrdersListDBManager(getContext());
        myAdapter = new MyAdapter(ordersListDBManager, this);

        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(myAdapter);

        if (chargeOrders ) {
            orderData = new OrderDataAsynchTask(OrdersFragment.recyclerView, getContext(), progressBar);
            orderData.execute();
            chargeOrders=false;
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        myAdapter.getData();

    }
}