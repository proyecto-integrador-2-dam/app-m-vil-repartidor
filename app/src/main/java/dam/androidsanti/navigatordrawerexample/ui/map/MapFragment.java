package dam.androidsanti.navigatordrawerexample.ui.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import dam.androidsanti.navigatordrawerexample.MainActivity;
import dam.androidsanti.navigatordrawerexample.R;
import dam.androidsanti.navigatordrawerexample.data.OrderDataAsynchTask;
import dam.androidsanti.navigatordrawerexample.data.OrdersListDBManager;
import dam.androidsanti.navigatordrawerexample.ui.orders.models.Order;

public class MapFragment extends Fragment implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;


    private static GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private FloatingActionButton fab;
    private FloatingActionButton fab2;
    private double currentLatitude;
    private double currentLongitude;
    private LatLng mypos;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private LocationManager mLocationManager;
    private View root;
    private ArrayList<Order> ordersList;
    private OrdersListDBManager ordersListDBManager;

    public static boolean checkPermissions=false;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_map, container, false);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        ((MainActivity) getActivity())
                .setActionBarTitle(getString(R.string.menu_map));
        fab = getActivity().findViewById(R.id.fab);
        fab.hide();
        fab2 = getActivity().findViewById(R.id.fab2);
        fab2.show();
        return root;
    }




    @SuppressLint("MissingPermission")
    private void setUI() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1000); // 1 second, in milliseconds

        long l = Long.valueOf(1);
        float f = Float.valueOf(1);
        mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, l, f, this);


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        System.out.println(checkPermissions);
        if (checkPermissions) {

            setUI();
        }
        LatLng almacen = new LatLng(38.6901941, -0.4969699);
        mMap.addMarker(new MarkerOptions().position(almacen).title(getString(R.string.almacen)).icon(BitmapDescriptorFactory.fromResource(R.drawable.warehouse_pin)));
        newMark();
        float zoom = 11;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(almacen, zoom));


    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (com.google.android.gms.location.LocationListener) this);

        } else {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();

            newMark();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        newMark();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void newMark() {
        mMap.clear();
        if (checkPermissions) {

            LatLng newpos = new LatLng(currentLatitude, currentLongitude);
            mypos = newpos;
            mMap.addMarker(new MarkerOptions().position(newpos).title(getString(R.string.ubicacio)).icon(BitmapDescriptorFactory.fromResource(R.drawable.truck_pin)));
        }
        addMarks();
    }

    public void addMarks() {
        LatLng almacen = new LatLng(38.6901941, -0.4969699);

        mMap.addMarker(new MarkerOptions().position(almacen).title(getString(R.string.almacen)).icon(BitmapDescriptorFactory.fromResource(R.drawable.warehouse_pin)));
        ordersListDBManager = new OrdersListDBManager(getContext());
        ordersList = ordersListDBManager.getOrders();

        for (int i = 0; i < OrderDataAsynchTask.listaOrders.size(); i++) {

            if (!ordersList.get(i).getEstado().equals(getResources().getString(R.string.delivered)) && !ordersList.get(i).getEstado().equals(getResources().getString(R.string.noDelivered))){

                mMap.addMarker(new MarkerOptions().position(new LatLng(OrderDataAsynchTask.listaOrders.get(i).getLatitud(), OrderDataAsynchTask.listaOrders.get(i).getLongitud()))
                        .title(OrderDataAsynchTask.listaOrders.get(i).getCliente()).icon(BitmapDescriptorFactory.fromResource(R.drawable.delivery_pin)));
            }
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);

            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("Error", "ERROR LOCATION");
        }
    }

    public static void changeMap(int i) {
        if (mMap != null) {
            mMap.setMapType(i);
        }
    }
}