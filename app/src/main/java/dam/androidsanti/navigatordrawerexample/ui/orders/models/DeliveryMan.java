package dam.androidsanti.navigatordrawerexample.ui.orders.models;

import java.io.Serializable;

public class DeliveryMan implements Serializable {

    public int id;
    public String nombre;
    public String apellidos;
    public String contrasenia;

    public DeliveryMan() {
    }

    public DeliveryMan(int id, String nombre, String apellidos, String contrasenia) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.contrasenia = contrasenia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    @Override
    public String toString() {
        return "DeliveryMan{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", contrasenia='" + contrasenia + '\'' +
                '}';
    }
}
