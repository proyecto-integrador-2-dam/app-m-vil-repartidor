package dam.androidsanti.navigatordrawerexample.ui.orders.models;

import java.io.Serializable;

public class Order implements Serializable {

    //private int id;
    private int idPedido;
    private String estado;
    private String observaciones;
    private String cliente;
    private String direccion;
    private double longitud;
    private double latitud;

    public Order() {
    }

    public Order(/*int id,*/ int idPedido, String estado, String observaciones, String cliente, String direccion) {
        //this.id = id;
        this.idPedido = idPedido;
        this.estado = estado;
        this.observaciones = observaciones;
        this.cliente = cliente;
        this.direccion = direccion;
    }

    /*public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }*/

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    @Override
    public String toString() {
        return "Order{" +
                //"id=" + id +
                ", idPedido=" + idPedido +
                ", estado='" + estado + '\'' +
                ", observaciones='" + observaciones + '\'' +
                ", cliente='" + cliente + '\'' +
                ", direccion='" + direccion + '\'' +
                ", longitud=" + longitud +
                ", latitud=" + latitud +
                '}';
    }
}