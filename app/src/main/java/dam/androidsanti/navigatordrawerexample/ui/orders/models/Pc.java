package dam.androidsanti.navigatordrawerexample.ui.orders.models;

public class Pc {

    public int id;
    public int pc;
    public int ciudad;

    public Pc() {
    }

    public Pc(int id, int pc, int ciudad) {
        this.id = id;
        this.pc = pc;
        this.ciudad = ciudad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPc() {
        return pc;
    }

    public void setPc(int pc) {
        this.pc = pc;
    }

    public int getCiudad() {
        return ciudad;
    }

    public void setCiudad(int ciudad) {
        this.ciudad = ciudad;
    }

    @Override
    public String toString() {
        return "Pc{" +
                "id=" + id +
                ", Pc=" + pc +
                ", ciudad=" + ciudad +
                '}';
    }
}
