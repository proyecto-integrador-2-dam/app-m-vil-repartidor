package dam.androidsanti.navigatordrawerexample.ui.orders.models;

public class Address {

    public int id;
    public String street;
    public int pc;

    public Address() {
    }

    public Address(int id, String street, int pc) {
        this.id = id;
        this.street = street;
        this.pc = pc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getPc() {
        return pc;
    }

    public void setPc(int pc) {
        this.pc = pc;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", street='" + street + '\'' +
                ", Pc=" + pc +
                '}';
    }
}
