package dam.androidsanti.navigatordrawerexample.ui.orders.models;

public class Rute {

    public int id;
    public int repartidor;

    public Rute(){

    }

    public Rute(int id, int repartidor) {
        this.id = id;
        this.repartidor = repartidor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRepartidor() {
        return repartidor;
    }

    public void setRepartidor(int repartidor) {
        this.repartidor = repartidor;
    }

    @Override
    public String toString() {
        return "Rute{" +
                "id=" + id +
                ", repartidor=" + repartidor +
                '}';
    }
}
