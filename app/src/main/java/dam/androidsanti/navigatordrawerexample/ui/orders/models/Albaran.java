package dam.androidsanti.navigatordrawerexample.ui.orders.models;

public class Albaran {

    public int id;
    public int pedido;
    public int ruta;

    public Albaran() {
    }

    public Albaran(int id, int pedido, int ruta) {
        this.id = id;
        this.pedido = pedido;
        this.ruta = ruta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPedido() {
        return pedido;
    }

    public void setPedido(int pedido) {
        this.pedido = pedido;
    }

    public int getRuta() {
        return ruta;
    }

    public void setRuta(int ruta) {
        this.ruta = ruta;
    }

    @Override
    public String toString() {
        return "Albaran{" +
                "id=" + id +
                ", pedido=" + pedido +
                ", ruta=" + ruta +
                '}';
    }
}
