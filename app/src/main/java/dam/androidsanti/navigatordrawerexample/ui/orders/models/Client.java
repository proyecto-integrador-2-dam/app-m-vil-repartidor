package dam.androidsanti.navigatordrawerexample.ui.orders.models;

public class Client {

    public int id;
    public String name;
    public String surnames;

    public Client() {
    }

    public Client(int id, String name, String surnames) {
        this.id = id;
        this.name = name;
        this.surnames = surnames;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurnames() {
        return surnames;
    }

    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surnames='" + surnames + '\'' +
                '}';
    }
}
