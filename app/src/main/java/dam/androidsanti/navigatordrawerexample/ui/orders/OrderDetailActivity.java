package dam.androidsanti.navigatordrawerexample.ui.orders;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import dam.androidsanti.navigatordrawerexample.R;
import dam.androidsanti.navigatordrawerexample.dao.OrderAPI;
import dam.androidsanti.navigatordrawerexample.data.OrdersListDBManager;

public class OrderDetailActivity extends AppCompatActivity {

    private Button btDone;
    private Button btUnrealized;
    private TextView tvState;
    private TextView tvReason;
    private TextView tvRes;
    private TextView nombreDestinatario;
    private TextView direccion;
    private TextView idPedido;
    private OrderAPI order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        setUI();
    }

    public void setUI() {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.order));

        nombreDestinatario = findViewById(R.id.tvNombreDestinatarioDetail);
        direccion = findViewById(R.id.tvDireccionDetail);
        idPedido = findViewById(R.id.tvIdPedidoDetail);
        btDone = findViewById(R.id.btDone);
        btUnrealized = findViewById(R.id.btUnrealized);
        tvState = findViewById(R.id.tvEstadoPedido);
        tvReason = findViewById(R.id.tvMotivo);
        tvRes = findViewById(R.id.tvMot);

        idPedido.setText(getIntent().getExtras().getInt("idpedido") + "");
        nombreDestinatario.setText(getIntent().getExtras().getString("nombredestinatario"));
        direccion.setText(getIntent().getExtras().getString("direccion"));
        tvState.setText(getIntent().getExtras().getString("estado"));
        tvReason.setText(getIntent().getExtras().getString("observaciones"));

        btDone.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (!tvState.getText().toString().equals(getResources().getString(R.string.delivered))) {

                    tvState.setText(getResources().getString(R.string.delivered));

                    tvReason.setText(getResources().getString(R.string.orderDelivered));
                    OrdersListDBManager ordersListDBManager = new OrdersListDBManager(getApplicationContext());
                    ordersListDBManager.updateOrder(tvState.getText().toString(), tvReason.getText().toString(), idPedido.getText().toString());
                    order = new OrderAPI();
                    try {
                        order.insert(Integer.valueOf(idPedido.getText().toString()), tvState.getText().toString(), tvReason.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.orderAlreadyDelivered), Toast.LENGTH_LONG).show();
                }

            }
        });

        btUnrealized.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    AlertDialog.Builder dialogOptions = new AlertDialog.Builder(OrderDetailActivity.this);

                    dialogOptions.setTitle(getResources().getString(R.string.indiReason));
                    dialogOptions.setIcon(R.drawable.del_cancel);

                    final CharSequence[] reasonsList = new CharSequence[4];
                    reasonsList[0] = getResources().getString(R.string.reason1);
                    reasonsList[1] = getResources().getString(R.string.reason2);
                    reasonsList[2] = getResources().getString(R.string.reason3);
                    reasonsList[3] = getResources().getString(R.string.reason4);

                    dialogOptions.setItems(reasonsList, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            tvReason.setText(reasonsList[which]);
                            tvState.setText(getResources().getString(R.string.noDelivered));

                            OrdersListDBManager ordersListDBManager = new OrdersListDBManager(getApplicationContext());
                            ordersListDBManager.updateOrder(tvState.getText().toString(), tvReason.getText().toString(), idPedido.getText().toString());
                            order = new OrderAPI();
                            try {
                                order.insert(Integer.valueOf(idPedido.getText().toString()), tvState.getText().toString(), tvReason.getText().toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    AlertDialog alertDialog = dialogOptions.create();
                    alertDialog.show();



            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

